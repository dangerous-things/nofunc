package nofunc


// Right structure
type Right struct {
	value interface{}
}

// Value is the getter of the Right wrapper
func (r Right) Value() interface{} {
	return r.value
}

// IsRight test if it's a Right
func (r Right) IsRight() bool {
	return true
}

// IsLeft test if it's a Left
func (r Right) IsLeft() bool {
	return false
}

// GetOrElse returns parameter value if Left else value of Right
func (r Right) GetOrElse(value interface{}) interface{} {
	return r.Value()
}

// Map on Right
func (r Right) Map(fn func(value interface{}) interface{}) interface{} {
	res := fn(r.value)
	if res == nil { // not sure of that ...
		return Either{}.OfLeft(nil)
	} else {
		return Either{}.OfRight(res)
	}
}

// Bind on Right
func (r Right) Bind(fn func(value interface{}) interface{}) interface{} {
	return fn(r.value)
}

// Cata on Right
func (r Right) Cata(leftFn func(value interface{}) interface{}, rightFn func(value interface{}) interface{}) interface{} {
	return rightFn(r.value)
}

//TODO: it's more an error than a value (?)

// Left structure
type Left struct {
	value interface{}
}

// IsRight test if it's a Right
func (l Left) IsRight() bool {
	return false
}

// IsLeft test if it's a Left
func (l Left) IsLeft() bool {
	return true
}

// Value is the getter of the Left wrapper
func (l Left) Value() interface{} {
	return l.value
}

// GetOrElse returns parameter value if Left else value of Right
func (l Left) GetOrElse(value interface{}) interface{} {
	return value
}

// Map on Left (returns always Left)
func (l Left) Map(fn func(value interface{}) interface{}) interface{} {
	return l
}

// Bind on Left (returns always Left)
func (l Left) Bind(fn func(value interface{}) interface{}) interface{} {
	return l // not sure
}

// Cata on Left
func (l Left) Cata(leftFn func(value interface{}) interface{}, rightFn func(value interface{}) interface{}) interface{} {
	return leftFn(l.value)
}


// Either structure
type Either struct {}

// OfRight : Right constructor
func (e Either) OfRight(value interface{}) Right {
	// TODO: test if null
	return Right{value:value}
}

// OfLeft : Left constructor
func (e Either) OfLeft(value interface{}) Left {
	return Left{value:value}
}

// FromNullable : Either constructor, returns interface then always test with switch
func (e Either) FromNullable(value interface{}) interface{} {
	if value == nil {
		return e.OfLeft(nil)
	} else {
		return e.OfRight(value)
	}
}



