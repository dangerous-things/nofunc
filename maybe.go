package nofunc

import (
	"errors"
)

// Some structure
type Some struct {
	value interface{}
}

// Value is the getter of the Some wrapper
func (s Some) Value() interface{} {
	return s.value
}

// IsSome test if it's a Some
func (s Some) IsSome() bool {
	return true
}

// IsNone test if it's a None
func (s Some) IsNone() bool {
	return false
}

// GetOrElse returns parameter value if None else value of Some
func (s Some) GetOrElse(value interface{}) interface{} {
	return s.Value()
}

// Map on Some
func (s Some) Map(fn func(value interface{}) interface{}) interface{} {
	res := fn(s.value)
	if res == nil {
		return Maybe{}.OfNone()
	} else {
		return Maybe{}.OfSome(res)
	}
}

// Bind on Some
func (s Some) Bind(fn func(value interface{}) interface{}) interface{} {
	return fn(s.value)
}


// None structure
type None struct {
	err error
}

// IsSome test if it's a Some
func (n None) IsSome() bool {
	return false
}

// IsNone test if it's a None
func (n None) IsNone() bool {
	return true
}

// ErrorMessage is the getter of the None wrapper
func (n None) ErrorMessage() error {
	return n.err
}

// GetOrElse returns parameter value if None else value of Some
func (n None) GetOrElse(value interface{}) interface{} {
	return value
}

// Map on None (returns always None)
func (n None) Map(fn func(value interface{}) interface{}) interface{} {
	return n
}

// Bind on None (returns always nil)
func (n None) Bind(fn func(value interface{}) interface{}) interface{} {
	return nil
}

// Maybe structure
type Maybe struct {
	//value interface{}
}

// OfSome : Some constructor
func (m Maybe) OfSome(value interface{}) Some {
	// TODO: test if null
	return Some{value:value}
}

// OfNone : None constructor
func (m Maybe) OfNone() None {
	return None{errors.New("😡 I'm nothing")}
}

// FromNullable : Maybe constructor, returns interface then always test with switch
func (m Maybe) FromNullable(value interface{}) interface{} {
	if value == nil {
		return m.OfNone()
	} else {
		return m.OfSome(value)
	}
}



